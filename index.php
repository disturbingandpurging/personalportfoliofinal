<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Personal Portfolio</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="wp-content/themes/mr_bok/style.css" media="screen, projection" rel="stylesheet" type="text/css" />
        <link href="stylesheets/print.css" media="print" rel="stylesheet" type="text/css" />
        <!--[if IE]>
        <link href="/stylesheets/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
        <![endif]-->
        <link href='https://fonts.googleapis.com/css?family=Crimson+Text' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Merriweather:300' rel='stylesheet' type='text/css'>
        <link href="https://file.myfontastic.com/n6vo44Re5QaWo8oCKShBs7/icons.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <section id="hero">
                <div class="top-nav">
                    <h1 class="logo"><a href="#">Rob Speakman</a></h1>
                    <button class="menu-button">
                    <span>Menu</span>
                    </button>
                    <nav class="menu-expanded">
                        <a href="#about" id="about-link">About</a>slickr-flickr tag=”bahamas”
                        <a href="#work" id="work-link">Work</a>
                        <a href="#blog" id="blog-link">Blog</a>
                        <a href="#contact" id="contact-link">Contact</a>
                    </nav>
                </div>
                <nav class="nav-wrap">
                    <a href="#" id="to-home"><span class="message">Home</span></a>
                    <a href="#about" id="to-about"><span class="message">About</span></a>
                    <a href="#work" id="to-work"><span class="message">Work</span></a>
                    <a href="#blog" id="to-blog"><span class="message">Blog</span></a>
                    <a href="#contact" id="to-contact"><span class="message">Contact</span></a>
                </nav>
                <h1 class="title">Rob Speakman</h1>
                <p class="subtext">User Experience Designer and Front End Web Developer</p>
                <a href="https://www.facebook.com/rob.speakman.9" class="icon"><span class="socicon-facebook"></span></a>
                <a href="https://twitter.com/RobSpeakmam" class="icon"><span class="socicon-twitter"></span></a>
                <a href="#" class="icon"><span class="socicon-linkedin"></span></a>
                <a href="https://www.instagram.com/robspeakman/" class="icon"><span class="socicon-instagram"></span></a>
                <a href="https://www.flickr.com/photos/53567691@N04/" class="icon"><span class="socicon-flickr"></span></a>
                <a href= "http://www.last.fm/user/RadioheadSnobclass=" class="icon"><span class="socicon-lastfm"></span></a>
                <div class="down-arrow-wrap">
                    <a href="#"><span class="down-arrow">&darr;</a>
                </span>
            </section>
        </div>
        <main>
        <section id="about">
            <div class="container">
                <div class="about-left">
                    <h2>About Rob</h2>
                    <p>
                        Hailing from Wigan in North West England, Rob Speakman is a User Experience Designer and Front End Developer, specialising in the creation of bespoke digital experiences. Having spent the past three years at Edge Hill University studying Web Systems Development, Rob has become adept at crafting responsive, standards compliant websites for a multitude of devices under user experience principles.
                    </p>
                    <p>
                        When not working on new mobile, tablet and desktop experiences, Rob is likely to be in a field with a camera, <a href="https://www.flickr.com/photos/53567691@N04/">taking captivating photographs</a>, creating artwork or <a href="http://www.last.fm/user/RadioheadSnob">listening to his extensive collection of music</a>.
                    </p>
                </div>
                <div class="about-right">
                <!--Wrap for P5.js canvas sketch-->
                    <div id="sketch-holder"></div>
                </div>
            </div>
        </section>
        <section id="work">
            <div class="container">
            <!--begin work row-->
                <div class="work-row">
                    <div class="work-left">
                        <img src="<?php bloginfo('template_url'); ?>/img/lappy1-min.png" alt="Macbook display of Experible system"/>
                    </div>
                    <div class="work-right">
                        <h2>Selected Works</h2>
                        <h3 class="work-title">Experible</h3>
                        <span class="tag">UX</span>
                        <span class="tag">Front-end development</span>
                        <span class="tag">Python</span>
                        <span class="tag">jQuery</span>
                        <span class="tag">Mercurial (Bitbucket)</span>
                        <span class="tag">Cognitive Walkthrough</span>
                        <span class="tag">Wireframes</span>
                        <span class="tag">Storyboards</span>
                        <span class="tag">Pyramid</span>
                        <p>Experible is an online user study experiment creation platform. As part of Rob's final year project, the Experible artifact was created using Python and the Pyramid Web Framework. Working in accordance to the Agile Sprint methodology and integrating User Centered Design principles to create prototypes and obtain feedback from actual end users through extensive user testing, the Experible system was created.</p>
                    </div>
                </div>
                <!--end work row-->
                <!--begin work row-->
                <div class="work-row">
                    <div class="work-left">
                        <h3>West Lancashire Fibromyalgia Support Group</h3>
                        <span class="tag">Charity</span>
                        <span class="tag">Responsive</span>
                        <span class="tag">Mobile First</span>
                        <span class="tag">Wordpress</span>
                        <span class="tag">Sass &amp; Susy</span>
                        <span class="tag">SEO</span>
                        <p>The founders of the West Lancashire Fibromyalgia Support Group required a fully responsive website for the charity that establishes an online presence to epouse its ethos throughout Lancashire. Using a responsive grid system using SASS and Susy, a mobile first oriented website was developed, in accordance with the client's requirements. </p>
                        <h3>
                        <a class="view-button" href="#">View</a>
                    </div>
                    <div class="work-right">
                        <img src="<?php bloginfo('template_url'); ?>/img/fibro-1-min.png" alt="Fibromalygia Support Group website on Imac"/>
                    </div>
                    </div><!--End work row-->
                </div>
            </section>
            <section id="blog">
            <!--Begin wordpress loop to echo post-->
            <?php query_posts('showposts=1'); ?>
                <?php if(have_posts()) :?>
                <?php while(have_posts()) : the_post();?>
                <article class="post">
                    <div class="container">
                        <h2 class="post-title"><a href="<?php the_permalink()?>"><?php the_title(); ?></h2>
                        <div class="post-content">
                            <p class="post-date">Posted on <?php the_date(); ?></p>
                            <p><?php the_content();?></p>
                        </div>
                            
                </article>
                <?php endwhile;?>
                <?php endif;?>
                <!--End wordpress loop-->
                    </div>
                </section>
                <section id="contact">
                    <div class="container">
                        <h2>Contact Me</h2>
                        <form action="#" method="post" id="contact-form">
                            <div class="input-row-half">
                                <label for="name">Your Name</label>
                                <input type="text" name="name" placeholder="Your Name">
                            </div>
                            <div class="input-row-half">
                                <label for="email">Email Address</label>
                                <input type="email" name="email" placeholder="you@youremail.com">
                            </div>
                            <div class="textarea-row-full">
                                <label for="message_body">Write your message</label>
                                <textarea rows="6" name="message_body"></textarea>
                            </div>
                            <div class="submit-button-wrap">
                                <input type="submit" value="Send">
                            </div>
                        </form>
                        
                    </div>
                </section>
                </main>
                <footer>
                    <div class="container">
                        <div class="copyright">Rob Speakman 2016&copy;</div>
                    </div>
                </footer>
            </body>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.5.0/p5.min.js"></script>
            <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/script.js"></script>
            <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/sketch.js"></script>
        </html>