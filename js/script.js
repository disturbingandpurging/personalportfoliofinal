window.onload = init;

function init(){
	var expandedMenu = document.getElementsByClassName('menu-expanded')[0];
	var toggleMenu = document.getElementsByClassName("menu-button")[0];
	var header = document.getElementsByClassName('top-nav')[0];
	
	/*Get the VH of the viewport*/
	var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	console.log(h);



	toggleMenu.addEventListener('click', function(e){
		e.preventDefault();
		console.log(expandedMenu);
		(toggleMenu.classList.contains("active") === true) ? toggleMenu.classList.remove("active") : toggleMenu.classList.add("active");
		if(toggleMenu.classList.contains("active")){
			expandedMenu.style.display = "block";
		} else {
			expandedMenu.style.display = "none";
		}

	});


/*Fixed header*/
	function stickyScroll(e) {
		if (window.pageYOffset > h) {
			header.classList.add('fixed');
		}

		if (window.pageYOffset < h) {
			header.classList.remove('fixed');

		}


	}

/*Listen for scroll event and fire stickyscroll function*/
	window.addEventListener('scroll', stickyScroll, false);

	
}